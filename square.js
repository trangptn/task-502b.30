import { Retangle } from "./retangle.js";

// Hình vuông kế thừa hình chữ nhật (Hình vuông là hình chữ nhật)
// Hình vuông có đầy đủ các thuộc tính và tính chất của hình chữ nhật
class Square extends Retangle {
    // Thuộc tính phát triển thêm
    _description;

    constructor(paramLength, paramDescription = "Hình vuông") {
        super(paramLength, paramLength);

        this._description = paramDescription;
    }

    getPerimeter() {
        return 4 * this._height;
    }
}

export { 
    Square
}