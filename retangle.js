class Retangle {
    _width;
    _height;

    constructor(paramWidth, paramHeight) {
        this._width = paramWidth;
        this._height = paramHeight;
    }

    getArea() {
        return this._width * this._height;
    }
}

export { Retangle }